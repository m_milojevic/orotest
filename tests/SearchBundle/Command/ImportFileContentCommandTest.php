<?php
/**
 * Created by Milos Milojevic(milosmoto@gmail.com).
 * User: milos
 * Date: 13.3.16.
 * Time: 22.51
 */

use SearchBundle\Command\ImportFileContentCommand;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use \Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ImportFileContentCommandTest extends KernelTestCase
{
    public function testExecute()
    {
        static::bootKernel();

        $container = static::$kernel->getContainer();
        $commandTask = new ImportFileContentCommand();
        $commandTask->setContainer($container);
        $application = new Application();
        $application->add($commandTask);


        $command = $application->find('database:import_files');
        $commandTester = new CommandTester($command);
        $commandTester->execute(array('command' => $command->getName()));

        $this->assertRegExp('/Checking file.../', $commandTester->getDisplay());
    }
}