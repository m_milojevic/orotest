<?php
/**
 * Created by Milos Milojevic(milosmoto@gmail.com).
 * User: milos
 * Date: 14.3.16.
 * Time: 21.34
 */

namespace Tests\SearchBundle\Service;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class SearchTest extends KernelTestCase
{
    public function testGetResultElastica()
    {
        static::bootKernel();

        $finder = static::$kernel->getContainer()->get('fos_elastica.finder.orotest_es.file');

        $results = $finder->find('test', 1);

        $this->assertNotEquals('', count($results));
    }
}