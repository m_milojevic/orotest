<?php
/**
 * Created by Milos Milojevic(milosmoto@gmail.com).
 * User: milos
 * Date: 14.3.16.
 * Time: 21.43
 */
namespace Tests\SearchBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SearchControllerTest extends WebTestCase
{
    public function testSearch()
    {
        $client = static::createClient();

        $crawler = $client->request('POST', '/', array('search' => 'test'));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Hello ORO!', $crawler->filter('p')->text());
        $this->assertContains('test', $crawler->filter('strong')->text());
        $this->assertGreaterThan(1, $crawler->filter('p')->count());
    }
}
