<?php
/**
 * Created by Milos Milojevic(milosmoto@gmail.com).
 * User: milos
 * Date: 13.3.16.
 * Time: 22.32
 */

namespace Tests\SearchBundle\Elastica;

use Elastica\Result;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class FileElasticaToModelTransformerTest extends KernelTestCase
{
    public function testTransform()
    {
        static::bootKernel();

        $service = static::$kernel->getContainer()->get('orotest.transformer.elastica.file');


        $elasticaObjects = array(
            new Result(array(
                '_source' => array(
                    'name' => 'test.txt',
                    'content' => 'test',
                    'id' => 1
                )
            ))
        );
        $results = $service->transform($elasticaObjects);

        $this->assertEquals(1, count($results));
        $this->assertEquals('test.txt', $results[0]['name']);
        $this->assertNotEquals(null, $results[0]['name']);
        $this->assertNotEquals(2, count($results));
    }
}