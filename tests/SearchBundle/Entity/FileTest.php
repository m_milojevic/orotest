<?php
/**
 * Created by Milos Milojevic(milosmoto@gmail.com).
 * User: milos
 * Date: 14.3.16.
 * Time: 21.50
 */

namespace Tests\SearchBundle\Entity;

use SearchBundle\Entity\File;

class SearchTest extends \PHPUnit_Framework_TestCase
{
    public function testGettersAndSettersName()
    {
        $file = new File();
        $file->setName('test.txt');

        $this->assertEquals('test.txt', $file->getName());
        $this->assertNotEquals('', $file->getName());
    }

    public function testGettersAndSettersPath()
    {
        $file = new File();
        $file->setPath('/test/test.txt');

        $this->assertEquals('/test/test.txt', $file->getPath());
        $this->assertNotEquals('', $file->getPath());
    }

    public function testGettersAndSettersParent()
    {
        $file = new File();
        $file->setParent(new File());

        $this->assertEquals(null, $file->getParent()->getId());
    }

    public function testGettersAndSettersChildren()
    {
        $file = new File();
        $file->setChildren(new File());

        $this->assertEquals(null, $file->getChildren()->getId());
    }

    public function testGettersAndSettersId()
    {
        $file = new File();

        $this->assertEquals(null, $file->getId());
        $this->assertNotEquals(1, $file->getName());

        $file->setId(1);

        $this->assertEquals(1, $file->getId());
        $this->assertNotEquals(null, $file->getId());
    }

    public function testGettersAndSettersCreatedAt()
    {
        $file = new File();

        $date = new \DateTime();

        $this->assertEquals(null, $file->getCreatedAt());
        $this->assertNotEquals($date, $file->getCreatedAt());

        $file->setCreatedAt($date);

        $this->assertEquals($date, $file->getCreatedAt());
        $this->assertNotEquals(null, $file->getCreatedAt());
    }

    public function testGettersAndSettersUpdatedAt()
    {
        $file = new File();

        $date = new \DateTime();

        $this->assertEquals(null, $file->getUpdatedAt());
        $this->assertNotEquals($date, $file->getUpdatedAt());

        $file->setUpdatedAt($date);

        $this->assertEquals($date, $file->getUpdatedAt());
        $this->assertNotEquals(null, $file->getUpdatedAt());
    }
}