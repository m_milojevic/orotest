<?php
/**
 * Created by Milos Milojevic(milosmoto@gmail.com).
 * User: milos
 * Date: 14.3.16.
 * Time: 19.30
 */

namespace SearchBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class SearchController extends Controller
{
    /**
     * @Route("/", name="search")
     * //TODO implement symfony form
     */
    public function searchAction(Request $request)
    {
        $query = $request->request->get('search', '');

        $service = $this->get($this->container->getParameter('search_type'));
        $results = $service->getResult($query);

        return $this->render('SearchBundle:Search:index.html.twig',
            array(
                'query' => $query,
                'results' => $results
            ));
    }
}