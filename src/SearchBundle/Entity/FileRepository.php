<?php
/**
 * Created by EtonDigital.
 * User: Milos Milojevic (milos.milojevic@etondigital.com)
 * Date: 15.3.16.
 * Time: 09.10
 */

namespace SearchBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * FileRepository
 *
 */
class FileRepository extends EntityRepository
{
    public function createIsParentQueryBuilder()
    {
        $queryBuilder = $this->createQueryBuilder('f');

        return $queryBuilder
            ->where('f.parent IS NULL');
    }
}
