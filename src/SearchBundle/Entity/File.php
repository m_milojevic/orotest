<?php
/**
 * Created by Milos Milojevic(milosmoto@gmail.com).
 * User: milos
 * Date: 14.3.16.
 * Time: 21.11
 */

namespace SearchBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * TODO Remove parent and children also recursive function, first testing optimization...
 * @ORM\Entity(repositoryClass="SearchBundle\Entity\FileRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table()
 */
class File {

    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $path;

    /**
     * @ORM\OneToOne(targetEntity="SearchBundle\Entity\File", mappedBy="children", cascade={"persist"})
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
     */
    private $parent;

    /**
     * @ORM\OneToOne(targetEntity="SearchBundle\Entity\File", cascade={"persist"})
     */
    private $children;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /** @ORM\Column(type="datetime", nullable=false) */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * */
    private $updated_at;

    /**
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime());
    }

    /**  @ORM\PrePersist */
    public function setCreatedAtValue()
    {
        $this->created_at = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return File
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return File
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     *
     * @return File
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return mixed
     */
    public function getESContent()
    {
        $content = $this->content;
        $this->getRecursiveContent($this, $content);

        return $content;
    }

    /**
     * @param mixed $content
     *
     * @return File
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     *
     * @return File
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     *
     * @return File
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     *
     * @return File
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param mixed $children
     *
     * @return File
     */
    public function setChildren($children)
    {
        $this->children = $children;
        return $this;
    }

    private function getRecursiveContent($parent, &$content)
    {
        if($parent->getChildren()){
            $content .= $parent->getChildren()->getContent();
            $this->getRecursiveContent($parent->getChildren(), $content);
        }

        return;
    }
}