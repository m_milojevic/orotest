<?php
/**
 * Created by Milos Milojevic(milosmoto@gmail.com).
 * User: milos
 * Date: 14.3.16.
 * Time: 20.31
 */

namespace SearchBundle\Interfaces;

interface SearchInterface
{
    public function getResult($query);
}