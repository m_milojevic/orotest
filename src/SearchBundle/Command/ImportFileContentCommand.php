<?php
/**
 * Created by Milos Milojevic(milosmoto@gmail.com).
 * User: milos
 * Date: 14.3.16.
 * Time: 21.20
 */

namespace SearchBundle\Command;

use Doctrine\DBAL\Configuration;
use SearchBundle\Entity\File;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Doctrine\DBAL\DriverManager;


class ImportFileContentCommand extends ContainerAwareCommand
{
    protected function configure() {
        $this
            ->setName('database:import_files')
            ->setDescription('Importing files content into database.');
    }

    /*
     * TODO Implement RabbiMQ, in case there is lot of files
     * TODO Implement removing no existing files, removed files also cascade delete children
     * Importing into mysql and elastic
     * Import limit 1GB , mysql longtext limitation.
     * update cli php.ini memory_limit with "-1"
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $connection = $em->getConnection();

        $finder = new Finder();
        $finder->files()->size('< 1073741824')->in(__DIR__ . '/../../../web/files');

        foreach ($finder as $file) {
            try{
                $output->writeln('Checking file...');
                $alreadyImported = $em->getRepository('SearchBundle:File')->findOneBy(array('name' => $file->getFileName()));

                if(!$alreadyImported && file_exists($file->getRealPath())){

                    $output->writeln('File import: ' . $file->getFileName());

                    if(filesize($file->getRealPath()) < 1048576){
                        $fileImportMaster = new File();
                        $fileImportMaster->setContent(@file_get_contents($file->getRealPath()))
                            ->setPath($file->getRealPath())
                            ->setName($file->getFileName());
                        $em->persist($fileImportMaster);
                        $em->flush();
                    }else{
                        $handle = @fopen($file->getRealPath(), "r");
                        if ($handle) {
                            $fileImportMaster = new File();
                            $fileImportMaster->setContent('')
                                ->setPath($file->getRealPath())
                                ->setName($file->getFileName());
                            $em->persist($fileImportMaster);
                            $em->flush();
                            $text = '';
                            while (!@feof($handle)) {
                                $getsText = @fgets($handle, 1048576);
                                if($getsText){
                                    $smallerText = $text;
                                    $text .= $getsText;
                                    if(strlen($text) > 1048576){
                                        $fileImport = new File();
                                        $fileImport->setContent($connection->quote($smallerText, \PDO::PARAM_STR))
                                            ->setPath($file->getRealPath())
                                            ->setParent($fileImportMaster)
                                            ->setName($file->getFileName());
                                        $em->persist($fileImport);
                                        $fileImportMaster->setChildren($fileImport);
                                        $em->persist($fileImportMaster);
                                        $fileImportMaster = $fileImport;
                                        $em->flush();
                                        $text = '';
                                    }
                                }
                            }
                            @fclose($handle);
                        }
                    }
                }
            }catch(\Exception $e){
                $output->writeln($e->getMessage());
            }
        }
    }
}