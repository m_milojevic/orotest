<?php
/**
 * Created by Milos Milojevic(milosmoto@gmail.com).
 * User: milos
 * Date: 14.3.16.
 * Time: 21.17
 */

namespace SearchBundle\Elastica;

use FOS\ElasticaBundle\Doctrine\ORM\ElasticaToModelTransformer;

/**
 * Class FileElasticaToModelTransformer
 */
class FileElasticaToModelTransformer extends ElasticaToModelTransformer
{
    /**
     * { @inheritdoc }
     */
    public function transform(array $elasticaObjects)
    {
        $results = array();
        foreach($elasticaObjects as $elasticResult){
            $results[] = array(
              'name' => $elasticResult->getSource()['name'],
            );
        }

        return $results;
    }
}