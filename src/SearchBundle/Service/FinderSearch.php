<?php
/**
 * Created by EtonDigital.
 * User: Milos Milojevic (milos.milojevic@etondigital.com)
 * Date: 16.3.16.
 * Time: 15.34
 */

namespace SearchBundle\Service;

use SearchBundle\Interfaces\SearchInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class FinderSearch implements  SearchInterface
{
    /*
     * Select search type in app/config/parameters.yml
     * TODO Implement pagination
     */
    public function getResult($query)
    {
        /*
         * Searching if files are smaller than php.ini memory limit. No need for database and elastic, searching from file with symfony finder
         * Only for very small number of files and very small files. Each file must be smaller than memory limit from php.ini
         */
        $finder = new Finder();
        $finder->files()->contains($query)->in(__DIR__ . '/../../../web/files');

        $results = array();

        foreach ($finder as $file) {
            $results[] = array(
                'name' => $file->getFileName()
            );
        }

        return $results;
    }
}