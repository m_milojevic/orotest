<?php
/**
 * Created by Milos Milojevic(milosmoto@gmail.com).
 * User: milos
 * Date: 14.3.16.
 * Time: 20.32
 */

namespace SearchBundle\Service;

use SearchBundle\Interfaces\SearchInterface;

class ElasticSearch implements  SearchInterface
{
    /*
     * @service Finder
     */
    private $finder;

    public function __construct($finder){
        $this->finder = $finder;
    }

    /*
     * Configure your search criteria
     * Select search type in app/config/parameters.yml
     * TODO Implement distinct agregation with elasticsearch query
     * TODO Implement pagination
     */
    public function getResult($query)
    {
        //Not so good solution, should be changed ...
        $results = $this->finder->find($query);
        $results = array_unique(
            array_map(
                function ($res) {
                    return $res['name'];
                }
                , $results
            )
        );

        $return = array();
        foreach ($results as $result) {
            $return[] = array(
                'name' => $result
            );
        }

        return $return;
    }
}